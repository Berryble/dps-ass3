create table election_log(
        id serial,

        source_bully_id numeric,
        source_address varchar(50),
        source_port numeric,

        destination_address varchar(50),
        destination_port numeric,

        message_type int,
        election_id uuid
);