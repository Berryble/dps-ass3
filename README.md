# Assignment 3

## Dependencies

In order to run the application you'll need: 

1. OpenJDK 11
2. Postgres

Additionally, to make the most out of it, you should run it under a Kubernetes cluster, but if Kubernetes is out of the equation follow steps defined in [How to run (Bare Metal)] otherwise refer to [How to run (Kubernetes Cluster)]


## How to run (Bare Metal)
[How to run (Bare Metal)]: #how-to-run-bare-metal

1. Change
```spring.datasource.url=jdbc:postgresql://ass3-db-service:5432/postgres``` property in src/main/resources/application.properties to ```spring.datasource.url=jdbc:postgresql://localhost:5432/postgres```
2. Execute the following commands to populate the database:
    ```bash
    cd database
    mvn flyway:migrate
    cd ..
    ```
3. Execute ```mvn clean install``` to build a runnable executable of the application
4. Execute the following commands to run the application
    ```bash
    cd target
    java -jar -Dserver.port=8080 -Dtcp.server.port=9999 ass3-0.0.1-SNAPSHOT.jar
    ```
5. Repeat step 4 with different server.port and tcp.server.port
6. Once you have at least 2 instances of the application running, execute the following command (or windows equivalent ``wget``) to start an election
   ```bash
   curl -X POST http://localhost:8080/api/v1/election/
   ```

## How to run (Kubernetes Cluster)
[How to run (Kubernetes Cluster)]: #how-to-run-kubernetes-cluster

(Replace kubectl with your favorite flavor of cluster)

1. Generate the persistence claim and start the database pod:
    ```sh
        kubectl apply -f kubernetes/database-configuration/postgres-claim.yaml
        kubectl apply -f kubernetes/database-configuration/postgres-deployment.yaml 
    ```
2. Once a database pod is up and running, forward the port 5432 (``kubectl port-forward <POD_ID> 5432``) and populate the database by executing the following commands from project directory:
   ```sh
    cd database
    mvn flyway:migrate
    cd ..
   ```
3. Start the back-end deployment by executing
   ```sh
   kubectl apply -f kubernetes/backend-configuration/backend-deployment.yaml
   ```
4. TBD