LOG_INFO="\e[32mINFO\e[0m"
LOG_WARNING="\e[33mWARNING\e[0m"
LOG_ERROR='\033[031mERROR\e[0m'
REQUIRED_BINARIES=('minikube' 'kubectl' 'mvn' 'docker')

function log(){
 echo -e "[$1] : $2" > /dev/tty
}
log "$LOG_INFO" "Checking binaries.."

function dependency_check() {
  for binary in "${REQUIRED_BINARIES[@]}" ; do
      if ! which "$binary" > /dev/null 2>&1; then
         printf '\t[\033[031m✗\e[0m] %s'" $binary" && exit 1
      else
         printf '\t[\e[32m✓\e[0m] %s'" $binary\n"
      fi
  done
}

#function build_database() {
#  kubectl get pods --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}' | grep db
#}
#
function build_backend() {
    log "$LOG_INFO" "Compiling backend"
    mvn clean install
    log "$LOG_INFO" "Copying artifacts"
    cp target/ass3-*.jar kubernetes/docker/backend-container/artifacts
    log "$LOG_INFO" "Building docker image"
    pushd kubernetes/docker/backend-container/ || exit
    docker build -t berryble/ass3-backend .
#    docker push berryble/ass3-backend can't do that one chief
    popd || exit
}

dependency_check
build_backend