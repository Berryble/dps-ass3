package me.t5.ass3.repository

import me.t5.ass3.model.Node
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface NodeRepository: JpaRepository<Node, Long> {

    fun findByBullyId(bullyId:Int):Node
    fun findByBullyIdGreaterThan(bullyId: Int):List<Node>
    fun findByPortAndAddress(port:Int, address:String): Node

}