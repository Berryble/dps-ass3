package me.t5.ass3.repository

import me.t5.ass3.model.election.ElectionLogEntry
import me.t5.ass3.model.election.MessageType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface LogRepository: JpaRepository<ElectionLogEntry, Long> {

    fun getElectionLogEntryByElectionIdEqualsAndMessageTypeIs(electionId: UUID, type:MessageType) : List<ElectionLogEntry>
    fun countElectionLogEntriesByElectionIdEqualsAndMessageTypeIs(electionId: UUID, type: MessageType) : Int
}