package me.t5.ass3.model.election

import java.util.*

data class Election(
    val electionId:UUID,
    val messages:ElectionMessageStatistics,
    val leaderId: Long,
    val bullyId: Int
    )