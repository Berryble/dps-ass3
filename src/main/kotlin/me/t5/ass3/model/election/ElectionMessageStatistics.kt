package me.t5.ass3.model.election

data class ElectionMessageStatistics(
    val electionCount:Int,
    val aliveCount:Int,
    val coordinatorCount:Int
)