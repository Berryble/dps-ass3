package me.t5.ass3.model.election

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "election_log")
data class ElectionLogEntry(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,

    @Column
    val sourceBullyId:Int,

    @Column
    val sourceAddress:String,

    @Column
    val sourcePort:Int,

    @Column
    val destinationAddress:String,

    @Column
    val destinationPort:Int,

    @Column
    val messageType: MessageType,

    @Column
    val electionId: UUID
)