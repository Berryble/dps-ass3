package me.t5.ass3.model.election

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

enum class MessageType {
    ALIVE,
    ELECTION,
    COORDINATOR;
}

data class BullyMessage(
    @JsonProperty("senderId")
    var senderId:Int,

    @JsonProperty("payload")
    var messageType: MessageType,

    @JsonProperty("electionId")
    var electionId:UUID)

