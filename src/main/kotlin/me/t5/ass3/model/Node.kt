package me.t5.ass3.model

import javax.persistence.*

// NEEDS KOTLIN JPA PLUGIN TO WORK
@Entity(name = "node")
data class Node(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    @Column(nullable = false)
    val address: String,
    @Column(nullable = false)
    val port: Int,
    @Column(nullable = false, name = "bullyid")
    val bullyId: Int
) {
}






