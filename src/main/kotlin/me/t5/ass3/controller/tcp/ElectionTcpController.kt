package me.t5.ass3.controller.tcp

import me.t5.ass3.configuration.client.ClientGateway
import me.t5.ass3.controller.tcp.utilities.TimerUtility
import me.t5.ass3.model.election.BullyMessage
import me.t5.ass3.model.election.MessageType
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import me.t5.ass3.model.Node
import me.t5.ass3.services.interfaces.LogService
import me.t5.ass3.services.interfaces.NodeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.event.EventListener
import org.springframework.integration.annotation.MessageEndpoint
import org.springframework.integration.annotation.ServiceActivator
import java.io.UncheckedIOException
import java.net.InetAddress
import java.util.*
import javax.annotation.PostConstruct
import kotlin.concurrent.schedule

@MessageEndpoint
class ElectionTcpController(@Autowired val clientGateway: ClientGateway,
                            @Autowired val nodeService: NodeService,
                            @Autowired val logService: LogService
) {
    private var LOGGER:Logger = LoggerFactory.getLogger(ElectionTcpController::class.java)
    private lateinit var selfNode:Node
    @Value("\${tcp.server.port}")

    private val selfPort = 0
    private var aliveTimer: Timer = Timer( true)
    private var lastElection: UUID = UUID.randomUUID()

    // I **REALLY** don't like this solution, but it kinda makes sense and makes the code look more clean so I will allow it, but do as I
    // say not as I do once again...
    @PostConstruct
    private fun init(){
        selfNode=nodeService.getSelf(InetAddress.getLocalHost().hostAddress, selfPort)
    }



    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @ServiceActivator(inputChannel = "bullyInputChannel")
    fun messageHandler(message: BullyMessage) {
        when (message.messageType) {
            MessageType.ELECTION -> {
                if(message.senderId < selfNode.bullyId ){
                    val senderNode = nodeService.getNodeByBullyId(message.senderId)
                    sendMessage(selfNode.bullyId, MessageType.ALIVE, message.electionId, senderNode.address, senderNode.port)
                    LOGGER.info("Election received from $senderNode, sending ALIVE")
                    electionListener(message.electionId)
                }
            }
            MessageType.ALIVE -> {
                LOGGER.info("ALIVE received from ${message.senderId}, waiting for COORDINATOR")
                aliveTimer = TimerUtility.recreateTimer(aliveTimer,10000L,false) {
                    if(logService.getLeaderMessage(message.electionId) == null){
                        LOGGER.warn("No coordinator received (${message.senderId}), restarting election ${message.electionId}")
                        electionListener(message.electionId)
                    }
                }
            }
            MessageType.COORDINATOR -> {
                LOGGER.info("COORDINATOR received, ${message.senderId} is now leader of ${selfNode.bullyId}")
                aliveTimer.cancel()
            }
        }
    }

    @EventListener
    fun electionListener(electionId:UUID) {
        val nodes = nodeService.getSuperiorNodes(selfNode.bullyId)
        aliveTimer = Timer(false)
        LOGGER.info("Starting election with ID $electionId")

        for (node in nodes) {
            LOGGER.info("Pinging $node")
            try{
                sendMessage(selfNode.bullyId, MessageType.ELECTION, electionId, node.address, node.port)
            } catch (e: UncheckedIOException){
                LOGGER.error("$node refused connection while ELECTING")
            }
        }

        aliveTimer.schedule(5000L){
            if(logService.getLeaderMessage(electionId) == null){
                LOGGER.info("{$selfNode.bullyId} has been elected as LEADER")
                for (node in nodeService.getAllNodes().filter { n -> selfNode.id != n.id  }) {
                    try {
                        sendMessage(selfNode.bullyId, MessageType.COORDINATOR, electionId, node.address, node.port)
                    } catch (e: UncheckedIOException) {
                        LOGGER.error("$node refused connection while COORDINATING")
                    }
                }
                lastElection = electionId
            }
        }
    }

    private fun sendMessage(bullyId:Int, messageType: MessageType, electionId: UUID, destinationAddress:String, destinationPort:Int){
        val bullyMessage = BullyMessage(bullyId, messageType, electionId)
        logService.logMessage(bullyMessage, selfNode.address, selfNode.port, destinationAddress, destinationPort)
        clientGateway.send(jacksonObjectMapper().writeValueAsString(bullyMessage), destinationAddress, destinationPort)
    }



}


