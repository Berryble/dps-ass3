package me.t5.ass3.controller.tcp.exceptions

class ElectionException(message: String?) : Throwable(message) {
}