package me.t5.ass3.controller.tcp.utilities

import java.util.*
import kotlin.concurrent.schedule

class TimerUtility {

    companion object{

        fun recreateTimer(oldTimer:Timer, delay:Long, isDaemon: Boolean = false, task: () -> Unit): Timer {
            oldTimer.cancel()
            oldTimer.purge()
            val timer = Timer("", isDaemon)
            timer.schedule(delay){
                task()
            }
            return timer
        }

    }
}