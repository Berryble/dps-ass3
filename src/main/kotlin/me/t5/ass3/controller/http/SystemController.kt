package me.t5.ass3.controller.http

import me.t5.ass3.model.Node
import me.t5.ass3.repository.NodeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*
import kotlin.concurrent.schedule
import kotlin.system.exitProcess


@RestController
@RequestMapping("api/v1/system")
class SystemController(
    @Autowired val nodeRepository: NodeRepository
) {

    @GetMapping("/nodes/")
    fun getMessages(): List<Node> {
        return nodeRepository.findAll()
    }

    @PostMapping("/suicide")
    fun terminate(): String {
        Timer(true).schedule(50L) {
            exitProcess(0)
        }
        return "TERMINATED"
    }


}