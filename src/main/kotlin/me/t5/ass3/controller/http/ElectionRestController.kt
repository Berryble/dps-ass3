package me.t5.ass3.controller.http

import me.t5.ass3.model.election.Election
import me.t5.ass3.model.election.ElectionMessageStatistics
import me.t5.ass3.model.election.MessageType
import me.t5.ass3.services.interfaces.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import org.springframework.web.util.UriComponentsBuilder;
import java.lang.IllegalArgumentException
import java.net.URI
import java.util.*

@RestController
@RequestMapping("api/v1/election")
class ElectionRestController(@Autowired val eventPublisher: ApplicationEventPublisher,
                                @Autowired val logService: LogService
) {

    @PostMapping()
    fun startElection(): ResponseEntity<String>{
        val electionId = UUID.randomUUID()
        eventPublisher.publishEvent(electionId)
        return ResponseEntity.accepted().body("${ServletUriComponentsBuilder.fromCurrentRequest().toUriString()}$electionId")
    }

    @GetMapping("/{electionId}/messages/{messageType}")
    fun getMessageCount(@PathVariable electionId: UUID, @PathVariable messageType: String):ResponseEntity<Int> {
        return try{
            ResponseEntity.ok().body(logService.getMessageCount(electionId, MessageType.valueOf(messageType.toUpperCase())))
        } catch (e :IllegalArgumentException){
            ResponseEntity.badRequest().body(-1)
        }
    }

    @GetMapping("/{electionId}/messages/")
    fun getMessageCount(@PathVariable electionId: UUID): ElectionMessageStatistics {
        return logService.getMessageStatistics(electionId)
    }

    @GetMapping("/{electionId}")
    fun getElection(@PathVariable electionId: UUID): Election {
        return logService.getElectionStatistics(electionId)
    }




}