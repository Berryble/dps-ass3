package me.t5.ass3.services

import me.t5.ass3.model.Node
import me.t5.ass3.repository.NodeRepository
import me.t5.ass3.services.interfaces.NodeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
class NodeServiceImpl(@Autowired val nodeRepository: NodeRepository): NodeService {

    // Can't cache otherwise won't be able to tell when a new node has come in
    override fun getAllNodes(): List<Node> {
        return nodeRepository.findAll()
    }

    @Cacheable("nodeByBullyId", key = "#bullyId")
    override fun getNodeByBullyId(bullyId: Int): Node {
        return nodeRepository.findByBullyId(bullyId)
    }


    override fun getSuperiorNodes(bullyIdBaseline: Int): List<Node> {
        return nodeRepository.findByBullyIdGreaterThan(bullyIdBaseline)
    }

    override fun registerNode(node: Node) {
        nodeRepository.save(node)
    }

    @Cacheable("nodeByBullyId", key = "#host+#port")
    override fun getSelf(host: String, port: Int): Node {
        return nodeRepository.findByPortAndAddress(port, host)
    }

    override fun deleteNode(node: Node) {
        nodeRepository.delete(node)
    }
}