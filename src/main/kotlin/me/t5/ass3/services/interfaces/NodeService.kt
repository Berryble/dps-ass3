package me.t5.ass3.services.interfaces

import me.t5.ass3.model.Node

interface NodeService {

    fun getAllNodes():List<Node>
    fun getNodeByBullyId(bullyId:Int):Node
    fun getSuperiorNodes(bullyIdBaseline: Int): List<Node>

    fun registerNode(node:Node)
    fun getSelf(host: String, port: Int): Node


    fun deleteNode(node: Node)
}