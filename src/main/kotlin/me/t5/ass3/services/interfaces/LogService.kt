package me.t5.ass3.services.interfaces

import me.t5.ass3.model.election.*
import java.util.*

interface LogService {

    fun logMessage(message: BullyMessage, sourceAddress:String, sourcePort:Int, destinationAddress: String, destinationPort: Int)
    fun getLeaderMessage(electionId: UUID): ElectionLogEntry?
    fun getMessageCount(electionId: UUID, messageType: MessageType): Int
    fun getMessageStatistics(electionId: UUID): ElectionMessageStatistics
    fun getElectionStatistics(electionId: UUID): Election
}