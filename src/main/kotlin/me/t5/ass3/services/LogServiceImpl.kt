package me.t5.ass3.services

import me.t5.ass3.model.election.*
import me.t5.ass3.repository.LogRepository
import me.t5.ass3.services.interfaces.LogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.util.*

@Service
class LogServiceImpl(@Autowired val messageRepository: LogRepository) : LogService {

    override fun logMessage(
        message: BullyMessage,
        sourceAddress: String,
        sourcePort: Int,
        destinationAddress: String,
        destinationPort: Int
    ) {
        messageRepository.save(
            ElectionLogEntry(
                0,
                message.senderId,
                sourceAddress,
                sourcePort,
                destinationAddress,
                destinationPort,
                message.messageType,
                message.electionId
            )
        )
    }

    override fun getLeaderMessage(electionId: UUID): ElectionLogEntry? {
        return try {
            messageRepository.getElectionLogEntryByElectionIdEqualsAndMessageTypeIs(electionId, MessageType.COORDINATOR)[0]
        }catch (e: EmptyResultDataAccessException){
            null
        }catch (e: IndexOutOfBoundsException){
            null
        }
    }

    override fun getMessageCount(electionId: UUID, messageType: MessageType): Int {
        return messageRepository.countElectionLogEntriesByElectionIdEqualsAndMessageTypeIs(electionId, messageType)
    }

    override fun getMessageStatistics(electionId: UUID): ElectionMessageStatistics {
        val electionCount = getMessageCount(electionId, MessageType.ELECTION)
        val aliveCount = getMessageCount(electionId, MessageType.ALIVE)
        val coordinatorCount = getMessageCount(electionId, MessageType.COORDINATOR)
        return ElectionMessageStatistics(electionCount, aliveCount, coordinatorCount)
    }

    override fun getElectionStatistics(electionId: UUID): Election {
        val messages = getMessageStatistics(electionId)
        val leader = getLeaderMessage(electionId)!!
        return Election(electionId, messages, leader.id, leader.sourceBullyId)
    }

}