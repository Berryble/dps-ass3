package me.t5.ass3.configuration.server

import me.t5.ass3.configuration.transformers.ObjectToMessageTransformer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.channel.DirectChannel
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.dsl.StandardIntegrationFlow
import org.springframework.integration.ip.dsl.Tcp
import org.springframework.integration.ip.dsl.TcpServerConnectionFactorySpec
import org.springframework.integration.ip.tcp.connection.TcpNetConnection
import org.springframework.messaging.MessageChannel


@Configuration
class TcpServerConfig {

    @Value("\${tcp.server.port}")
    private val port= 0

    @Bean
    fun electionListener() : StandardIntegrationFlow {
        return IntegrationFlows.from(Tcp.inboundGateway(Tcp.netServer(port)))
            .transform(ObjectToMessageTransformer())
            .channel("bullyInputChannel")
            .get()
    }

}

