package me.t5.ass3.configuration.transformers

import me.t5.ass3.model.election.BullyMessage
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.integration.transformer.AbstractPayloadTransformer

class ObjectToMessageTransformer: AbstractPayloadTransformer<ByteArray, BullyMessage>() {

    override fun transformPayload(payload: ByteArray): BullyMessage {
        val mapper = ObjectMapper()
        return mapper.readValue(payload)
    }
}