package me.t5.ass3.configuration.client

import org.springframework.integration.annotation.MessagingGateway
import org.springframework.messaging.handler.annotation.Header

@MessagingGateway(defaultRequestChannel = "tcpElectionClient.input")
interface ClientGateway {

    fun send(message:String, @Header("host") host:String, @Header("port") port: Int)

}