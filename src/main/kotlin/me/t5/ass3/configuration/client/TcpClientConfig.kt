package me.t5.ass3.configuration.client

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.dsl.IntegrationFlowDefinition

@Configuration
class TcpClientConfig {

    @Bean
    fun tcpElectionClient(): IntegrationFlow {
        return IntegrationFlow {
                f: IntegrationFlowDefinition<*> -> f.route(TcpRouter())
        }
    }

}