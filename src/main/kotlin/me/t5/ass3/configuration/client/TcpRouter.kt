package me.t5.ass3.configuration.client

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.dsl.IntegrationFlowDefinition
import org.springframework.integration.dsl.context.IntegrationFlowContext
import org.springframework.integration.ip.tcp.TcpSendingMessageHandler
import org.springframework.integration.ip.tcp.connection.TcpNetClientConnectionFactory
import org.springframework.integration.router.AbstractMessageRouter
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel


class TcpRouter : AbstractMessageRouter() {

    private val MAX_CACHED = 10 // When this is exceeded, we remove the LRU.


    private val subFlows: LinkedHashMap<String, MessageChannel> = object : LinkedHashMap<String, MessageChannel>(
        MAX_CACHED, .75f, true
    ) {
        override fun removeEldestEntry(eldest: Map.Entry<String, MessageChannel>): Boolean {
            return if (size > MAX_CACHED) {
                removeSubFlow(eldest)
                true
            } else {
                false
            }
        }
    }

    @Autowired
    private val flowContext: IntegrationFlowContext? = null

    @Synchronized
    override fun determineTargetChannels(message: Message<*>): Collection<MessageChannel> {
        var channel =
            subFlows[message.headers.get("host", String::class.java) + message.headers["port"]]
        if (channel == null) {
            channel = createNewSubflow(message)
        }
        return listOf(channel)
    }

    private fun createNewSubflow(message: Message<*>): MessageChannel {
        val host = message.headers["host"] as String
        val port = message.headers["port"] as Int
        val hostPort = host + port
        val cf = TcpNetClientConnectionFactory(host, port)
        val handler = TcpSendingMessageHandler()
        handler.setConnectionFactory(cf)
        val flow = IntegrationFlow { f: IntegrationFlowDefinition<*> -> f.handle(handler) }
        val flowRegistration = flowContext!!.registration(flow)
            .addBean(cf)
            .id("$hostPort.flow")
            .register()
        val inputChannel = flowRegistration.inputChannel
        subFlows[hostPort] = inputChannel
        return inputChannel
    }

    private fun removeSubFlow(eldest: Map.Entry<String, MessageChannel>) {
        val hostPort = eldest.key
        flowContext!!.remove("$hostPort.flow")
    }


}