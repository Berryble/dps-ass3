package me.t5.ass3.configuration.lifecycle

import me.t5.ass3.model.Node
import me.t5.ass3.services.interfaces.NodeService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import java.net.InetAddress
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy
import kotlin.random.Random

@Configuration
class NodeLifecycleHandler(@Autowired val nodeService: NodeService) {

    @Value("\${tcp.server.port}")
    private val PORT= 0
    private val LOGGER = LoggerFactory.getLogger(NodeLifecycleHandler::class.java)
    lateinit var node:Node

    @PostConstruct
    fun init(){
        val allNodes = nodeService.getAllNodes()
        val ip = InetAddress.getLocalHost().hostAddress

        var bullyId: Int
//        bullyId=-1;

        while (true){
            bullyId=Random.nextInt(1, 100)
            if(allNodes.none { node -> node.bullyId == bullyId }){
                break
            }
        }

        if(PORT == 9999){
            bullyId = -1
        }

        node = Node(0, ip,PORT, bullyId)
        nodeService.registerNode(node)

        LOGGER.info("Registering: $node ")
    }


    @PreDestroy
    fun onExit(){
        nodeService.deleteNode(node)
        LOGGER.info("Deleting: $node ")
    }


}